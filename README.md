# azrs-tracking

Pracenje koriscenja alata za razvoj softvera za kurs "Alati za razvoj softvera" na MATF-u.
Repozitorijum predstavlja prikaz primene alata za razvoj softvera na projektu "SobaZabave" koji je radjen na kursu Razvoj softvera.

Projekat na kome su primenjivani alati može se pronaći na sledećem [linku](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/11-SobaZabave).

# Prikazani alati

1. [Git hooks](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/10)

2. [GDB](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/2)

3. [Git](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/1)

4. [CMake](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/3)

5. [ClangTidy](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/4)

6. [ClangFormat](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/5)

7. [Valgrind](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/9)

8. [Docker](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/7)

9. [GammaRay](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/6)

10. [GCov](https://gitlab.com/Dzondzi/azrs-tracking/-/issues/8) 

